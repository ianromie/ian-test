import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  titles = 'test';

  constructor( private title: Title,
    private meta: Meta) {

    }

    ngOnInit() {
      this.title.setTitle('Dynamic Hello Angular Lovers Title');
        this.meta.updateTag({ 'property': 'og:site_name', content: 'The Planet Reviews' })
        this.meta.updateTag({ 'property': 'og:url', content: 'http://theplanetreviews.com.chamaeleon.webstarterz.com/reviews/22/' })
        this.meta.updateTag({ 'property': 'og:type', content: 'article' })
        this.meta.updateTag({ 'property': 'og:title', content: 'Dell i7559' })
        this.meta.updateTag({ 'property': 'og:description', content: 'Dell,Electronic Devices,Laptops By Maker,Dell' })
        this.meta.updateTag({ 'property': 'og:image', content: 'http://34.87.133.204:5001/api/photo/review/afb56243-1735-44ea-bcc1-0cd58aacea47-928103.jpg' })
        this.meta.updateTag({ 'property': 'og:image:width', content: '200' })
        this.meta.updateTag({ 'property': 'og:image:height', content: '200' })
        this.meta.updateTag({ 'property': 'og:image:og:image:secure_url', content: 'http://34.87.133.204:5001/api/photo/review/afb56243-1735-44ea-bcc1-0cd58aacea47-928103.jpg' })        
    }
}
